﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(YamiWebuSaito.Startup))]
namespace YamiWebuSaito
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
