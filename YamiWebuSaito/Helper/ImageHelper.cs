﻿using System;
using System.Web.Mvc;

namespace YamiWebuSaito.Helper
{
    public static class ImageHelper
    {
        public static MvcHtmlString Image(this HtmlHelper helper, string id, string url, string alternateText)
        {
            // Create tag builder 
            var builder = new TagBuilder("img");

            // Create valid id 
            builder.GenerateId(id);

            // Add attributes 
            builder.MergeAttribute("src", url);
            builder.MergeAttribute("alt", alternateText);

            // Render tag 
            return new MvcHtmlString(builder.ToString(TagRenderMode.SelfClosing));
        }
    }
}