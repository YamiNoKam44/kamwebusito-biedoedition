﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace YamiWebuSaito.Models
{
    public class Blogi
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Tresc { get; set; }
        public string Title { get; set; }
        public string Kategoria { get; set; }
    }
    public class BlogiContext : ApplicationDbContext
    {
        public DbSet<Blogi> Blogi { get; set; }
    }
}