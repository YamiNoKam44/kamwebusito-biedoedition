﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;


namespace YamiWebuSaito.Models
{
    public class Kategoria
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Nazwa Kategorii")]
        [DataType(DataType.Text)]
        [MaxLength(16)]
        public string Nazwa { get; set; }

        public string Opis { get; set; }
    }

    public class KategoriaContext : ApplicationDbContext
    {
        public DbSet<Kategoria> Kategorie { get; set; }
    }
}