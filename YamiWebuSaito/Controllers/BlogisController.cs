﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using YamiWebuSaito.Models;

namespace YamiWebuSaito.Controllers
{
    public class BlogisController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Blogis
        public ActionResult Index()
        {
            return View(db.Blogis.ToList());
        }

        // GET: Blogis/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blogi blogi = db.Blogis.Find(id);
            if (blogi == null)
            {
                return HttpNotFound();
            }
            return View(blogi);
        }

        // GET: Blogis/Create
        public ActionResult Create()
        {
                var lista = from k in db.Kategorias
                            select k.Nazwa;
            ViewBag.lista = lista.ToList();
            return View();
        }

        // POST: Blogis/Create
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Description,Tresc,Title,Kategoria")] Blogi blogi)
        {
            if (ModelState.IsValid)
            {
                db.Blogis.Add(blogi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(blogi);
        }

        // GET: Blogis/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blogi blogi = db.Blogis.Find(id);
            if (blogi == null)
            {
                return HttpNotFound();
            }
            return View(blogi);
        }

        // POST: Blogis/Edit/5
        // Aby zapewnić ochronę przed atakami polegającymi na przesyłaniu dodatkowych danych, włącz określone właściwości, z którymi chcesz utworzyć powiązania.
        // Aby uzyskać więcej szczegółów, zobacz https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Description,Tresc,Title,Kategoria")] Blogi blogi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(blogi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(blogi);
        }

        // GET: Blogis/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blogi blogi = db.Blogis.Find(id);
            if (blogi == null)
            {
                return HttpNotFound();
            }
            return View(blogi);
        }

        // POST: Blogis/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Blogi blogi = db.Blogis.Find(id);
            db.Blogis.Remove(blogi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
